require "./spec_helper"

describe "span_as_string" do
  it "works" do
    span = Time::Span.new(days: 2)
    span_as_string(span).should eq "2d"
    span = Time::Span.new(hours: 2)
    span_as_string(span).should eq "2h"
    span = Time::Span.new(hours: 2, seconds: 20)
    span_as_string(span).should eq "2h20s"
  end
end
