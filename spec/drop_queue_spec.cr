require "./spec_helper"

describe DropQueue do
  it "works" do
    int_deque = DropQueue(Int32).new(3)
    int_deque.push(1)
    int_deque.push(2)
    int_deque.push(3)
    int_deque.push(4)
    int_deque.push(5)
    int_deque.deque.size.should eq 3
    int_deque.deque[0].should eq 3
    int_deque.deque[1].should eq 4
    int_deque.deque[2].should eq 5
  end
end
