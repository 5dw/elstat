# Incidents API

The Incidents API allows administrators of elstat to publish custom
information regarding the current state of the system.

They can create an incident and add stages to it as the incident
progresses via the API.

## Authentication

Authentication to the Incidents API is simple, as it doesn't need
to be flexible to have user/password authentication.

In an instance's `config.py`, hash the value in the `PASSWORD` variable,
using SHA256, encode the value as a hex digest and put the result
in the `Authorization` header.

### Example

`PASSWORD` = `"abc123"`

Headers:

```json
Authorization: 6ca13d52ca70c883e0f0bb101e425a89e8624de51db2d2392593af6a84118090
```

## Objects

### Incident object

```javascript
incident_type = "outage" | "partial_outage" | "degraded_service";

incident = {
  // id of the incident, an uuid
  id: str,

  // check incident_type, only specific strings are allowed
  incident_type: incident_type,

  // Straightforward.
  title: str,

  // could be edited later to put a postmortem of the incident.
  content: str,

  // if the incident is still ongoing and should
  // be displayed in the front page
  ongoing: bool,

  // When the incident started, as a unix timestamp in milliseconds
  start_timestamp: int,

  // When it finished, nullable
  end_timestamp: int,

  // should be ordered by their timestamp, first being the oldest
  stages: [stage],
};
```

### Stage object

```javascript
{
    title: str,
    content: str,

    // unix timestamp in milliseconds
    timestamp: int,
}
```

## API Errors

All errors will have this payload.

```javascript
{
    error: true,
    code: int,
    message: string,
}
```

### API Error codes

| code | meaning                                                |
| ---- | ------------------------------------------------------ |
| 400  | bad request, your payload is invalid                   |
| 401  | unauthorized, password not provided / invalid password |

## The API

### `GET /api/incidents/current`

**Does not require authentication.**

Get the current status of incident information. Shows the current ongoing
incident, if any.

Returns `null` or an incident object.

### `GET /api/incidents/<page:int>`

**Does not require authentication.**

Get historical information about incidents given a page. Each page
contains 10 incidents.

Returns a list of incident objects, it can be empty.

### `POST /api/incidents`

Create a new incident and put it as the current ongoing incident
by default.

#### Input

Partial incident object (only `type`, `title`, `content`).

#### Output

The full incident object.

### `PATCH /api/incidents/<incident_id:int>`

Change information about any incident.

#### Input

Partial incident object, with all its keys being optional.
Fields in partial are: `type`, `title`, `content`, `ongoing`,
`start_date`, `end_date`.

Clients using the Incident API can mark `ongoing` as `false` to close
an ongoing incident. `end_date` will be set as the current date.

#### Output

Nothing, with 204 status code.

### `POST /api/incidents/<incident_id:int>/stages`

Create a new stage in an incident.

**Note that you can't edit stage objects.**

#### Input

Partial stage object. Fields: `title`, `content`

#### Output

Full stage object.
