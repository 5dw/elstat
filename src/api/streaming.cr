require "json"
require "kemal"
require "logger"
require "uuid"
require "../helpers"

enum MessageOP
  Unsubscribe  = -1
  Subscribe    =  0
  Subscribed   =  1
  Unsubscribed =  2
  Data         =  3

  # incident specific
  IncidentNew    = 4
  IncidentUpdate = 5
  IncidentClose  = 6

  # optimization for large channel instances
  SubscribeAll = 7
end

enum ErrorCodes
  InvalidPayload = 4200
  TooMuch        = 4420
end

class Message
  include JSON::Serializable

  @[JSON::Field(key: "op")]
  property op : Int32

  @[JSON::Field(key: "channels")]
  property channels : Array(String)?
end

def ws_sendchan(ws, opcode : MessageOP, channels : Array(String))
  ws.send({op: opcode.to_i32, channels: channels}.to_json)
end

add_context_storage_type(UUID)

def parse_and_act(data, ws, env, manager)
  log = manager.context.log
  message = Message.from_json(data)
  opcode = MessageOP.new(message.op)

  client_id = (env.get? "client_id").as(UUID?)

  if opcode == MessageOP::Subscribe || opcode == MessageOP::SubscribeAll
    # if the client is sending a Subscribe for the first time,
    # we allocate a new client ID and set it on env.
    # if not, we just resubscribe with the given list.
    if client_id.nil?
      client_id = UUID.random
      log.info("new client: #{client_id}")
      env.set "client_id", client_id
    end

    subscribed = if opcode == MessageOP::SubscribeAll
                   manager.streaming.subscribe_all ws, client_id
                 else
                   manager.streaming.subscribe ws, client_id, message.channels.as(Array(String))
                 end
    ws_sendchan(ws, MessageOP::Subscribed, subscribed)
  elsif opcode == MessageOP::Unsubscribe
    if client_id.nil?
      raise Kemal::WebsocketError.new(
        ErrorCodes::InvalidPayload.to_i32, "Not subscribed")
    end

    manager.streaming.unsubscribe client_id, message.channels.as(Array(String))
  else
    raise Kemal::WebsocketError.new(
      ErrorCodes::InvalidPayload.to_i32, "Invalid op code")
  end
end

def handle_ws_message(data, ws, env, manager)
  log = manager.context.log

  if data.size > 2048
    raise Kemal::WebsocketError.new(
      ErrorCodes::TooMuch.to_i32, "Too much data")
  end

  begin
    parse_and_act(data, ws, env, manager)
  rescue ex : Kemal::WebsocketError
    raise ex
  rescue ex : Exception
    log.error("Websocket failed: #{ex}")
    raise Kemal::WebsocketError.new(4000, "err: #{ex.message}")
  end
end

def websocket_start(ws, env, manager)
  ws.on_message do |msg|
    handle_ws_message(msg, ws, env, manager)
  end

  ws.on_close do
    client_id = env.get? "client_id"

    if client_id.is_a?(UUID)
      unsubbed = manager.streaming.rm_websocket(client_id)
    end
  end
end
