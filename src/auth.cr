require "kemal"
require "digest"

require "./helpers"

macro do_login
  auth = env.request.headers["Authorization"]?
  if auth.nil?
    halt env, status_code: 401, response: "Unauthorized"
  end

  manager = get_manager(env)
  cfg = manager.@context.cfg
  orig_pwd = cfg["elstat"]["password"]
  
  hash = Digest::SHA256.new
  hash.update(orig_pwd)
  orig_hash = hash.final.hexstring

  if auth != orig_hash
    halt env, status_code: 401, response: "Invalid password"
  end
end
