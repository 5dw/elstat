# A drop queue drops the items at the end of itself when a new item comes
# in that would overflow the queue.
#
# It uses a deque internally as:
# > It performs better than an Array when there are frequent
# > insertions or deletions of items near the beginning or the end.
class DropQueue(T)
  property deque : Deque(T)
  property max_size : UInt64

  def initialize(@max_size : UInt64)
    @deque = Deque(T).new(max_size)
  end

  def initialize(@max_size : UInt64, existing_array : Array(T))
    @deque = Deque(T).new(existing_array)
  end

  def deque
    @deque
  end

  def push(value : T)
    while @deque.size >= @max_size
      @deque.shift
    end

    @deque.push value
  end
end
