require "./manager"

class StreamingManager
  @channels = {} of String => Set(WebsocketID)
  @websockets = {} of WebsocketID => HTTP::WebSocket

  property context : Context

  def initialize(@context)
  end

  CHANNEL_PREFIXES = ["status", "latency"]

  def create_channel(channel_name : String)
    @channels[channel_name] = Set(WebsocketID).new
    @context.log.info("Created channel '#{channel_name}'")
  end

  def create_service_channels(service_name : String)
    CHANNEL_PREFIXES.each do |prefix|
      create_channel("#{prefix}:#{service_name}")
    end
  end

  # Subscribe the given websocket (with its client ID) to a list of channels.
  # Registers the websocket's Client ID into the internal websocket mapping.
  # Returns the list of channels that the client successfully subscribed
  # to.
  def subscribe(ws, client_id, channels : Array(String)) : Array(String)
    @websockets[client_id] = ws
    subscribed = [] of String

    channels.each do |channel|
      subs = @channels[channel]?
      if !subs.nil?
        subs.add(client_id)
        subscribed.push(channel)
      end
    end

    @context.log.info("subscribed #{client_id} to #{subscribed.size} channels")
    subscribed
  end

  def subscribe_all(ws, client_id) : Array(String)
    @websockets[client_id] = ws
    subscribed = [] of String

    @channels.each do |channel, _subscriber_set|
      subscribed.push(channel)
    end

    @context.log.info("subscribing #{client_id} to (all) #{subscribed.size} channels")
    subscribe(ws, client_id, subscribed)
  end

  # Unsubscribe the given WebsocketID from the given list of channels.
  def unsubscribe(
    client_id : WebsocketID, channels : Array(String)
  ) : Array(String)
    unsubbed = [] of String

    channels.each do |channel|
      subs = @channels[channel]?

      if (!subs.nil?) && subs.delete(client_id)
        unsubbed.push(channel)
      end
    end

    @context.log.info("unsubbed #{client_id} from #{unsubbed}")
    return unsubbed
  end

  # Remove the given WebsocketID from all channels.
  def rm_websocket(client_id : WebsocketID) : Array(String)
    unsubbed = unsubscribe(client_id, @channels.keys)

    # We remove the client id from @websockets AFTER the unsubscribe()
    # call to not cause havoc considering concurrency.
    @websockets.delete(client_id)
    return unsubbed
  end

  def dispatch_to(channel_name : String, data : String)
    @channels[channel_name].each do |client_id|
      ws = @websockets[client_id]
      begin
        ws.send(data)
      rescue ex : IO::Error
        @context.log.info("unsubbing #{client_id} from all channels (error = #{ex}")

        # An IO error happened. Remove the websocket from our client list
        rm_websocket(client_id)
      end
    end
  end
end
