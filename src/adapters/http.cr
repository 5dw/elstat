require "http"
require "./adapter"
require "../status_codes"

class HTTPAdapter < Adapter
  # Get a human-friendly phrase describing the error code.
  def self.get_phrase(status_code : String) : String
    text = STATUS[status_code]?
    return text || "Unknown status"
  end

  def self.query(ctx, serv_cfg)
    start = Time.monotonic
    headers = HTTP::Headers.new
    headers["User-Agent"] = serv_cfg["http_user_agent"]? || "Crystal/elstat"

    serv_cfg.each do |key, value|
      if !key.starts_with?("http_header.")
        next
      end

      parts = key.split(".")
      header_key = parts[1]
      headers[header_key] = value
    end

    begin
      resp = HTTP::Client.get(
        serv_cfg["http_url"],
        headers: headers,
      )
    rescue ex
      raise AdapterError.new("http error: #{ex.message}")
    end

    elapsed = (Time.monotonic - start).total_milliseconds.to_i64
    status = resp.status_code

    success = status == 200 || status == 204

    # NOTE: those are here for quick development
    # purposes. we can't auto unit-test this code.
    # success = false
    # raise "piss"

    if !success
      elapsed = 0.to_f64
      status_phrase = self.get_phrase(status.to_s)
      raise AdapterError.new("http status #{status} - #{status_phrase}")
    end

    return AdapterResult.new(success, elapsed)
  end
end
