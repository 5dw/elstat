require "./adapter"

require "db"
require "mysql"
require "pg"

class SQLAdapter < Adapter
  def self.query(ctx, serv_cfg)
    start = Time.monotonic

    begin
      conn = DB.open(serv_cfg["url"])
    rescue ex
      raise AdapterError.new("sql connect error: #{ex.message}")
    end

    begin
      conn.exec serv_cfg["query"]
    rescue ex
      raise AdapterError.new("sql exec error: #{ex.message}")
    ensure
      conn.close
    end

    elapsed = (Time.monotonic - start).total_milliseconds.to_i64
    return AdapterResult.new(true, elapsed)
  end
end
