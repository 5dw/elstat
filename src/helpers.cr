require "kemal"
require "logger"
require "./manager"

add_context_storage_type(Logger)
add_context_storage_type(Manager)

def get_logger(env)
  log_opt = (env.get? "log").as(Logger?)
  unless log_opt.nil?
    log_opt
  else
    puts "no manager found, this is an assertion failure"
    exit 1
  end
end

def get_manager(env)
  manager_opt = (env.get? "manager").as(Manager?)
  unless manager_opt.nil?
    manager_opt
  else
    puts "no manager found, this is an assertion failure"
    exit 1
  end
end

def span_as_string(span : Time::Span)
  final_string = ""

  if span.days > 0
    final_string += "#{span.days}d"
  end
  if span.hours > 0
    final_string += "#{span.hours}h"
  end
  if span.minutes > 0
    final_string += "#{span.minutes}m"
  end
  if span.seconds > 0
    final_string += "#{span.seconds}s"
  end

  final_string
end

def span_as_downtime_string(span : Time::Span)
  "down for #{span_as_string(span)} (#{span.total_seconds}s)"
end
