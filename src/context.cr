require "ini"
require "logger"
require "db"
require "sqlite3"

class Context
  property cfg = INI.parse(File.read("config.ini"))
  property log = Logger.new(STDOUT)
  property db : DB::Database = DB.open("sqlite3://elstat.db")

  def setup
    root = cfg["elstat"]
    if root["debug"]? == "1"
      log.info("setting logger to debug")
      log.level = Logger::DEBUG
    end
  end

  def thresholds(serv_name : String) : NamedTuple(slow: UInt64, down: UInt64, latency: UInt64)
    serv = @cfg["service:#{serv_name}"]
    global = @cfg["elstat"]

    slow_threshold = (serv["slow_threshold"]? || global["slow_threshold"]).to_u64
    down_threshold = (serv["down_threshold"]? || global["down_threshold"]).to_u64
    latency_threshold = (serv["slow_threshold_ms"]? || global["slow_threshold_ms"]).to_u64

    {slow: slow_threshold, down: down_threshold, latency: latency_threshold}
  end
end
